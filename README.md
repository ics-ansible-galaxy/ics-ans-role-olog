# ics-ans-role-olog

Ansible role to install olog.

## Role Variables

```yaml
olog_network: olog-network

olog_container_name: olog
olog_container_image: "{{ olog_container_image_name }}:{{ olog_container_image_tag }}"
olog_container_image_name: registry.esss.lu.se/ics-software/olog
olog_container_image_tag: latest
olog_container_image_pull: True
olog_env: {}
olog_frontend_rule: "Host:{{ ansible_fqdn }}"

olog_database_container_name: olog-database
olog_database_image: postgres:9.6.7
olog_database_volume: olog-database
olog_database_name: olog
olog_database_username: olog
olog_database_password: olog
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-olog
```

## License

BSD 2-clause
